package pl.stopzone.shop.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class PublicController {

    @RequestMapping
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView("public/index");
        modelAndView.addObject("content", "indexPublic");
        modelAndView.addObject("title", "Strona startowa");
        return modelAndView;
    }

}
